# PQWS Structural

Mechanical parts that enclosing the electronics of the PQWS weather station.

## Instructions

Clone the repository with --recursive to make sure lsf-kicad-lib submodule is initialiazed.

## License

Licensed under the [CERN OHLv1.2](LICENSE) 2018 Libre Space Foundation.